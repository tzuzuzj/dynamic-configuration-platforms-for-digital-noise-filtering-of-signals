Copyright 2020 Stefan Dangl (stefan.dangl@tuwien.ac.at), 
License: ISC license

-------------------------------------------------------------------------------------
Pre-requisites
-------------------------------------------------------------------------------------

Hardware:
- Altera DE2-70 FPGA-Board  
- Wondom DSP Board 
- PJRC Teensy 3.6 MCU
- arbitrary Windows PC

Software: 
- Python 3
- Octave 5.1.0.0
- Quartus II 13.0 (newer versions may not work with the recomended FPGA-Board)
- Sigma Studio 4.4
- Arduino 1.8.10 + teensy.exe


-------------------------------------------------------------------------------------
Preperation-Step
-------------------------------------------------------------------------------------

1)
Connect your Windows PC with the MCU.

2)
Open the Arduino Project "\repo\noise_detection.ino"

3)
choose: Tools -> Board -> Teensy 3.6

4)
Upload the Program.


-------------------------------------------------------------------------------------
FPGA-Implementation
-------------------------------------------------------------------------------------

1)
Connect your Windows PC with the FPGA-Board.

2)
Open the Quartus II Project "\repo\Quartus_Project\AutomatedDigitalFilter.qpf".
Start the Compilation, open the programmer and start the programming of the FPGA-Board.

3)
Open the FPGA-script with a text editor and adjust the 15th line following:
change
"start "C:\Users\Programme\GNU_Octave_CLI --persist C:\Users\repo\scripts\filter_coefficients.m"
to	
start <your absolute program path of GNU Octave CLI> --persist <your absolute path of the repository>\scripts\filter_coefficients.m

4)
Run the FPGA-script. The filter instrument should now adjust his behaviour automatically depending on the input signal.
If the script doesn't work correctly check if you connected the MCU with the correct COM port.

5)
To run the files on a Unix machine only the bash script has to be changed slightly.


--------------------------------------------------------------------------------------
DSP-Implementation
--------------------------------------------------------------------------------------

1)
Connect your Windows PC with the DSP-Board.

2) 
Open the Sigma Studio Project "\repo\SigmaStudio_Project\AutomatedDigitalFilter.dspproj".
Select: Tools -> Script. A new window should open.
Select at the new window: File -> Open -> "repo\scripts\SigmaStudio_script.sss" -> Tools -> Run Script

3)
Start the DSP-Script. The filter instrument should now adjust his behaviour automatically depending on the input signal.
If the script doesn't work correctly check if you connected the MCU with the correct COM port.

4)
Unfortunately Sigma Studio is not available for Unix systems.