library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.snes_cntrl_pkg.all;

entity snes_cntrl is
	generic (
		CLK_FREQ : integer := 50_000_000;
   	constant CLK_OUT_FREQ : integer := 1_000_000;
		constant BIT_TIME : integer := 50
	);
	port (
		clk : in std_logic;
		res_n : in std_logic; 
		snes_latch : out std_logic;
		snes_clk : out std_logic;
		snes_data : in std_logic;
		button_state : out snes_buttons_t
	);
end entity;



architecture beh of snes_cntrl is

	signal shiftreg : std_logic_vector(15 downto 0) := (others => '0');
	signal shiftreg_next : std_logic_vector(15 downto 0) := (others => '0');
	signal state_next : snes_states_t := WAIT_TIMEOUT;
	signal state : snes_states_t;
	signal clk_cnt : integer := 0;
	signal bit_cnt : integer := 0;
	signal clk_cnt_next : integer := 0;
	signal bit_cnt_next : integer := 0;
	constant REFRESH_TIMEOUT : integer := 400_000;
	signal button_state_next : snes_buttons_t := (others => '1');
	
begin
	
	next_state : process(all)
	begin
	
		state_next <= state;
		bit_cnt_next <= bit_cnt;
		clk_cnt_next <= clk_cnt;
	
		case state is
			when WAIT_TIMEOUT =>
				if clk_cnt = REFRESH_TIMEOUT then
					clk_cnt_next <= 0;
					state_next <= LATCH;
				else
					clk_cnt_next <= clk_cnt + 1;
					bit_cnt_next <= 0;
				end if;
				
			when LATCH =>
				if clk_cnt = BIT_TIME/2 then
					clk_cnt_next <= 0;
					state_next <= LATCH_WAIT;
				else
					clk_cnt_next <= clk_cnt + 1;
					bit_cnt_next <= 0;
				end if;
				
			when LATCH_WAIT =>
				if clk_cnt = BIT_TIME/2 then
					clk_cnt_next <= 0;
					state_next <= CLK_LOW;
				else
					clk_cnt_next <= clk_cnt + 1;
					bit_cnt_next <= 0;
				end if;
				
			when CLK_LOW =>
				if clk_cnt = BIT_TIME/2-1 then
					clk_cnt_next <= 0;
					state_next <= SAMPLE;
				else
					clk_cnt_next <= clk_cnt + 1;
				end if;
				
			when SAMPLE =>
				state_next <= CLK_HIGH;

			when CLK_HIGH =>
				if bit_cnt /= 15 and clk_cnt = BIT_TIME/2 then
					bit_cnt_next <= bit_cnt + 1;
					clk_cnt_next <= 0;
					state_next <= CLK_LOW;
				elsif bit_cnt = 15 and clk_cnt = BIT_TIME/2 then
					clk_cnt_next <= 0;
					bit_cnt_next <= 0;
					state_next <= WAIT_TIMEOUT;
				else
					clk_cnt_next <= clk_cnt + 1;
				end if;
				
				when others =>
					
		end case;
	end process;
	
	
	output_state : process(all)
	begin
	
		shiftreg_next <= shiftreg;
		button_state_next <= button_state;

		case state is
			when WAIT_TIMEOUT =>
				snes_latch <= '0';
				snes_clk <= '1';

				button_state_next.btn_b      <= shiftreg(15);
				button_state_next.btn_y      <= shiftreg(14);
				button_state_next.btn_select <= shiftreg(13);
				button_state_next.btn_start  <= shiftreg(12);
				button_state_next.btn_up     <= shiftreg(11);
				button_state_next.btn_down   <= shiftreg(10);
				button_state_next.btn_left   <= shiftreg(9);
				button_state_next.btn_right  <= shiftreg(8);	
				button_state_next.btn_a      <= shiftreg(7);
				button_state_next.btn_x      <= shiftreg(6);
				button_state_next.btn_l      <= shiftreg(5);
				button_state_next.btn_r      <= shiftreg(4);
	
			when LATCH =>
				snes_latch <= '1';
				snes_clk <= '1';
				
			when LATCH_WAIT =>
				snes_latch <= '0';
				snes_clk <= '1';
				
			when CLK_LOW =>
				snes_latch <= '0';
				snes_clk <= '0';
				
			when SAMPLE =>
				snes_clk <= '0';
				snes_latch <= '0';
				shiftreg_next (15 downto 1) <= shiftreg(14 downto 0);
				shiftreg_next(0) <= not snes_data;
				
			when CLK_HIGH =>
				snes_latch <= '0';
				snes_clk <= '1';
				
			when others =>
			
		end case;
	end process;
	
	
	sync_state : process(clk, res_n)
	begin
		if not res_n then
			bit_cnt <= 0;
			clk_cnt <= 0;
			state <= WAIT_TIMEOUT;
			shiftreg <= (others => '0');
		elsif rising_edge(clk) then
			state <= state_next;
			clk_cnt <= clk_cnt_next;
			bit_cnt <= bit_cnt_next;
			shiftreg <= shiftreg_next;
			button_state <= button_state_next;
		end if;
		
	end process;
end architecture;



