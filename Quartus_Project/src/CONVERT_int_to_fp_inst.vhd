CONVERT_int_to_fp_inst : CONVERT_int_to_fp PORT MAP (
		clk_en	 => clk_en_sig,
		clock	 => clock_sig,
		dataa	 => dataa_sig,
		result	 => result_sig
	);
