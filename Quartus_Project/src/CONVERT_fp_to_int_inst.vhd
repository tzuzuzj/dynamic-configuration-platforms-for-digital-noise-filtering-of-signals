CONVERT_fp_to_int_inst : CONVERT_fp_to_int PORT MAP (
		clk_en	 => clk_en_sig,
		clock	 => clock_sig,
		dataa	 => dataa_sig,
		result	 => result_sig
	);
