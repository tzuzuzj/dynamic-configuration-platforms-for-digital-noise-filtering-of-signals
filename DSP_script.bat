# Use this file for the automated filtering via DSP-board

cls
cd scripts

echo 0 0 0 0 0 > FilterInformation.txt
mode COM3 BAUD=9600 PARITY=n DATA=8

:DSP_loop	
	echo 1 > COM3
	COPY COM3 FilterInformation.txt
	TIMEOUT 1
goto DSP_loop