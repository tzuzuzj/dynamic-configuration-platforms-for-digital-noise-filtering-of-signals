# Use this file for the automated filtering via FPGA-board

cls

cd scripts
mode COM3 BAUD=9600 PARITY=n DATA=8
serialPort_init.py

:loop
	TIMEOUT 1
	echo 1 > COM3
	COPY COM3 FilterInformation.txt	
	type FilterInformation.txt
	TIMEOUT 1
	start C:\Users\Programme\GNU_Octave_CLI --persist C:\Users\repo\scripts\filter_coefficients.m
	TIMEOUT 3
	serialPort.py
	goto loop